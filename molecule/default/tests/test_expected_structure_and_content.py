import pytest


@pytest.mark.parametrize("filename", [
    ("/home/ansible/.ssh/id_deployment_key"),
    ("/home/ansible/.ssh/config"),
])
def test_expected_structure(host, filename):
    file = host.file(filename)
    assert file.exists
    assert 'ansible' == file.user
    assert 'ansible' == file.group
    assert 0o600 == file.mode


def test_expected_key_content(host):
    assert get_expected_key_content() \
           == host.file('/home/ansible/.ssh/id_deployment_key') \
           .content_string.strip()


def get_expected_key_content():
    return """-----BEGIN OPENSSH PRIVATE KEY-----
b3BlbnNzaC1rZXktdjEAAAAABG5vbmUAAAAEbm9uZQAAAAAAAAABAAAAMwAAAAtzc2gtZW
QyNTUxOQAAACBS+Jx/XIFuy6/JKL//zyrcrTG5XvjiRm4AbeYTmK9FVgAAAKA9u3H3Pbtx
9wAAAAtzc2gtZWQyNTUxOQAAACBS+Jx/XIFuy6/JKL//zyrcrTG5XvjiRm4AbeYTmK9FVg
AAAEDJBHrV1YHOL87lSMoU2whLpCmekvLlNC6TXWEAXUrw6VL4nH9cgW7Lr8kov//PKtyt
Mble+OJGbgBt5hOYr0VWAAAAGnRlc3RpbmcgZml4dHVyZSBkZXBsb3kga2V5AQID
-----END OPENSSH PRIVATE KEY-----"""


def test_expected_ssh_config_content(host):
    assert get_expected_ssh_config_content() \
           == host.file('/home/ansible/.ssh/config').content_string


def get_expected_ssh_config_content():
    return """# BEGIN ANSIBLE MANAGED BLOCK
Host gitlab.com
HostName gitlab.com
User git
Port 22
IdentityFile /home/ansible/.ssh/id_deployment_key
# END ANSIBLE MANAGED BLOCK"""
