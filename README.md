# macrominds/provision/uberspace/deployment-key

Put a private deployment key onto a server to be able to checkout private git repos. 

## Requirements

Setup an [U7 Uberspace](https://dashboard.uberspace.de/register) and provide an ssh access to it.

Generate a private/public key pair: 

`ssh-keygen -t ed25519 -C "a comment describing the key" -f ~/.ssh/id_deployment_key`

You can absolutely set a password for that key if you are using macrominds/website with
autodeployment via deployer/deployer. Deployer will ask you for the password.

Adapt the filename as you wish, you just need to configure the filenames later on. 

Then add the public key to your repository hoster. In case of gitlab:

Open https://gitlab.com/{your-repo}/-/settings/repository and expand _Deploy Keys_. 

Enter a title and paste the content of your public key. 

`cat ~/.ssh/id_deployment_key.pub` and copy paste. 

If you have xclip installed, you can directly put it into your clipboard: 
`xclip -sel clip < ~/.ssh/id_deployment_key.pub`

Press __Add key__. 

## Role Variables

See [defaults/main.yml](defaults/main.yml). 

* `deployment_key_local_path`: full path to local key file
  Defaults to `'{{ deployment_key_local_base_path }}/{{ deployment_key_local_private_file_name }}'`
* `deployment_key_local_base_path`: parent path of local key file 
  Defaults to `'.'`
* `deployment_key_local_private_file_name`: name of the local key file 
  Defaults to `'id_deployment_key'`

* `deployment_key_remote_path`: where to put the key file on the server
  Defaults to `'{{ deployment_key_remote_base_path }}/{{ deployment_key_local_private_file_name }}'`
* `deployment_key_remote_base_path`: parent path of key file on the server
  Defaults to `'/home/{{ ansible_facts.user_id }}/.ssh'`

* `deployment_key_remote_ssh_config`: where to find the ~/.ssh/config on the server
  Defaults to `'{{ deployment_key_remote_base_path }}/config'`

* `deployment_key_ssh_config_host`: `Host` entry of ~/.ssh/config
  Defaults to `gitlab.com`
* `deployment_key_ssh_config_host_name`: `HostName` entry of ~/.ssh/config
  Defaults to `'{{ deployment_key_ssh_config_host }}'`
* `deployment_key_ssh_config_user`: `User` entry of ~/.ssh/config
  Defaults to `git`
* `deployment_key_ssh_config_port`: `Port` entry of ~/.ssh/config
  Defaults to `22`

See [test file](molecule/default/tests/test_expected_structure_and_content.py) 
for expected ~/.ssh/config content

## Example Playbook

### Prerequisites

In your project, provide the following files:

ansible.cfg

```ini
[defaults]
roles_path = $PWD/galaxy_roles:$PWD/roles
```

requirements.yml
 
```yaml
- src: git+https://gitlab.com/macrominds/provision/uberspace/deployment-key.git
  path: roles
  name: deployment-key
```

And run `ansible-galaxy install -r requirements.yml` to install 
or `ansible-galaxy install -r requirements.yml  --force` to upgrade.

### Playbook

```yaml
- hosts: all
  roles:
    - role: deployment-key
      deployment_key_local_path: ~/.ssh/id_deployment_key
```

## Testing

Test this role with `molecule test --all`.

## License

ISC

## Author Information

This role was created in 2019 by Thomas Praxl.
