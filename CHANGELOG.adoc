= Change Log

All noteable changes to this project will be documented in this file.
This project adheres to http://semver.org/[Semantic Versioning].

== [unreleased]

* fix: molecule lint format ( https://molecule.readthedocs.io/en/latest/configuration.html#lint )
* fix: molecule driver: containers for docker & podman support

== [untagged] – 2019-12-13

* basic functionality
